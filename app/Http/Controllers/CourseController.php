<?php

namespace App\Http\Controllers;
use App\Models\Course;
use App\Models\CourseLesson;
use App\Models\UserProgress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class CourseController extends Controller
{

    // Return the home
    public function getHome(){

        $courses = Course::select('title', 'slug', 'description', 'image_src', 'course_id')
        ->where('is_active', true)
        ->get();

        $user = Auth::user();

        // Check the status of each course
        foreach($courses as $course){

            if($user!=null){

                $check = UserProgress::where('user_id', $user->user_id)
                ->where('course_id', $course->course_id)
                ->whereNull('course_lesson_id')
                ->take(1)
                ->get();

                if(isset($check[0])){
                    $course->status = 1;
                }else{
                    $course->status = 0;
                }

            }

        }

        return view('home', ['courses'=>$courses]);
    }

    // Return the home
    public function getCourseBySlug($slug, $lessonId=null){

        // Get the course
        $course = Course::select('title', 'slug', 'description', 'image_src', 'course_id')
        ->where('is_active', true)
        ->where('slug', $slug)
        ->take(1)
        ->get();

        if(isset($course[0]) && $course = $course[0]){
            // If lessonId is null, we will look for the first lesson in the course
            if($lessonId==null){
                $searchLessonId = CourseLesson::where('course_id', $course->course_id)
                ->where('is_active', true)
                ->orderBy('outstanding_order', 'asc')
                ->take(1)
                ->pluck('course_lesson_id')
                ->toArray();
                $lessonId = $searchLessonId[0];
            }
        }else{
            return redirect()->route('get.home')->with('error', trans('web.course_not_exist_error'));
        }

        // Lesson of the course
        $lesson = CourseLesson::where('course_lesson_id', $lessonId)
        ->where('course_id', $course->course_id)
        ->where('is_active', true)
        ->orderBy('outstanding_order', 'asc')
        ->take(1)
        ->get();

        if(isset($lesson[0]) && $lesson = $lesson[0]){

            if($lesson->lesson_type==1){
                $cloudflareVideoId = json_decode($lesson->lesson_data, true)['id'];
                $htmlContent = CourseLesson::getVideoFromCloudflareStream($cloudflareVideoId);
            }else{
                $htmlContent = $lesson->lesson_text;
            }

            // Next Lesson
            $nextLessonQuery = CourseLesson::select('course_lesson_id', 'title')
            ->where('course_id', $course->course_id)
            ->where('course_lesson_id', '<>', $lesson->course_lesson_id)
            ->where('outstanding_order', '>', $lesson->outstanding_order)
            ->where('is_active', true)
            ->orderBy('outstanding_order', 'asc')
            ->take(1)
            ->get();

            $nextLesson = $nextLessonQuery[0] ?? null;

            // Previous Lesson
            $previousLessonQuery = CourseLesson::select('course_lesson_id', 'title')
            ->where('course_id', $course->course_id)
            ->where('course_lesson_id', '<>', $lesson->course_lesson_id)
            ->where('outstanding_order', '<', $lesson->outstanding_order)
            ->where('is_active', true)
            ->orderBy('outstanding_order', 'desc')
            ->take(1)
            ->get();

            $previousLesson = $previousLessonQuery[0] ?? null;

            // Assign points to the user
            $user = Auth::user();

            // Error, user must be login
            if($user==null){
                return redirect()->route('get.home')->with('error', trans('web.not_login_error'));
            }

            $check = UserProgress::where('user_id', $user->user_id)
            ->where('course_lesson_id', $lesson->course_lesson_id)
            ->take(1)
            ->get();

            if(count($check)==0){
                UserProgress::firstOrCreate([
                    'user_id'=>$user->user_id,
                    'course_lesson_id'=>$lesson->course_lesson_id
                ],
                [
                    'xp_points'=>800,
                    'course_id'=>$course->course_id
                ]);
                User::where('user_id', $user->user_id)->increment('xp_points', 800);
            }

            // Percentage
            $totalContents = CourseLesson::select('course_id')->where('course_id', $course->course_id)->get();
            $userProgress = UserProgress::select('course_id')->where('course_id', $course->course_id)->whereNotNull('course_lesson_id')->get();

            $percentage = round(count($userProgress)/count($totalContents) * 100);

            // Course completed
            if($percentage==100){
                $check = UserProgress::where('user_id', $user->user_id)
                ->where('course_id', $course->course_id)
                ->whereNull('course_lesson_id')
                ->take(1)
                ->get();

                if(count($check)==0){
                    UserProgress::insert([
                        'user_id'=>$user->user_id,
                        'xp_points'=>1600,
                        'course_id'=>$course->course_id
                    ]);
                    User::where('user_id', $user->user_id)->increment('xp_points', 1600);
                }

            }

            // Return the view
            return view('course', [
                'course'=>$course,
                'currentLesson'=>$lesson,
                'previousLesson'=>$previousLesson,
                'nextLesson'=>$nextLesson,
                'htmlContent' => $htmlContent,
                'percentage' => $percentage,
            ]);

        }else{
            return redirect()->route('get.home')->with('error', trans('web.course_not_exist_error'));
        }

    }

}
