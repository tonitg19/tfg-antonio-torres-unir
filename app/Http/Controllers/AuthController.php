<?php

namespace App\Http\Controllers;

use App\Helpers\Web3;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    // This function receive the request with the user wallet and generate a message to sign
    public function getGenerateMesage(Request $request){

        $wallet = $request->input('wallet');

        $response = Web3::generate_message(['address'=>$wallet]);

        // save the message encrypted
        $textPlainMessage = encrypt($response['message']);

        return response()->json(array_merge(['encrypted_message'=>$textPlainMessage], $response));

    }

    // --
    public function postCheckSignature(Request $request){

        // Wallet, nonce and signature
        $wallet = $request->input('wallet');
        $signature = $request->input('signature');
        $message = decrypt($request->input('encrypted_message'));

        // Check the signature
        if (!Web3::verify_signature($message, $signature, $wallet)){

            return response()->json(['result'=>'ko']);

        }else{

            $user = User::firstOrCreate([
                'wallet'=>$wallet,
            ]);

            Auth::loginUsingId($user->user_id);

            return response()->json(['result'=>'ok']);

        }

    }

    // logout
    public function getLogout(){

        Auth::logout();
        return redirect()->route('get.home');

    }

}
