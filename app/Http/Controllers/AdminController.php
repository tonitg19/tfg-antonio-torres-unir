<?php

namespace App\Http\Controllers;

use App\Models\AdminList;
use App\Models\AdminModal;
use App\Models\Course;
use App\Models\CourseLesson;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{

    public function getAllCourses(){

        $coursesOut = Course::select('*')
        ->get();

        // We make a HTML table
        $adminList = new AdminList($coursesOut ?? []);

        $adminList->addColumn('check', 'course_id', '');

        $adminList->addColumn('thumbnail', 'image_src', trans('admin.thumbnail'));
        $adminList->addColumn('text', 'course_id', trans('admin.id'));
        $adminList->addColumn('bold-text', 'title', trans('admin.title'));
        $adminList->addColumn('status', 'status', trans('admin.status'));
        $adminList->addColumn('text', 'created_at', trans('admin.published'));

        $adminList->addColumn('options', 'course_id', trans('admin.options'), [
            'elements' => [
                [
                    'title' => trans('admin.course_edit'),
                    'icon' => 'fa-solid fa-pen-to-square',
                    'onclick' => 'editCourse',
                    'onclick_params' => 'course_id'
                ],
                [
                    'title' => trans('admin.course_content_list'),
                    'icon' => 'fa-solid fa-list-ul',
                    'route'=> 'get.admin.course.content.by.id'
                ],
                [
                    'title' => trans('admin.delete_course'),
                    'icon' => 'fa-solid fa-trash-can',
                    'onclick' => 'deleteCourse',
                    'onclick_params' => 'course_id'
                ]
            ]
        ]);

        $table = $adminList->getTable();

         // Edit modal
         $adminModal = new AdminModal("editCourseModal", 'Editar curso', route('post.admin.course.by.id'), 'modal-xl');

         // Start first column
         $adminModal->addInput('start_column', 'first_column', '');
 
         $adminModal->addInput('hidden', '_token', '', ['value'=>csrf_token()]);
         $adminModal->addInput('hidden', 'course_id', 'course_id');
         $adminModal->addInput('text', 'title', trans('admin.title'));
         $adminModal->addInput('text', 'slug', trans('admin.slug'));
         $adminModal->addInput('text', 'description', trans('admin.description'));
         $adminModal->addInput('text', 'image_src', trans('admin.image_src'));

        // End first column & start second column
        $adminModal->addInput('end_column', 'first_column', '');
        $adminModal->addInput('start_column', 'second_column', '');

        $adminModal->addInput('text', 'outstanding_order', trans('admin.outstanding_order'), ['validation'=>'number']);
        $adminModal->addInput('select', 'is_active', trans('admin.is_active'), ['select'=>
            [0=>trans('admin.inactive'), 1=>trans('admin.active')]
        ]);
        $adminModal->addInput('submit', 'submit', trans('admin.update'));
 
         // End second column
         $adminModal->addInput('end_column', 'second_column', '');
 
         $modal = $adminModal->getModal();

        return view('admin.courses-list', [
            'table'=>$table,
            'modal'=>$modal
        ]);

    }

    public function putCreateNewCourse(){

        Course::create([
            'slug'=>'change-me-'.Str::random(6),
            'title'=>'',
            'description'=>'',
            'is_active'=>false
        ]);

        return redirect()->route('get.admin.courses');
        
    }

    public function getContentFromCourse(Request $request, $courseId){

        $courseOut = Course::select('*')
        ->where('course_id', $courseId)
        ->take(1)
        ->get();

        $lessonsOut = CourseLesson::select('*')
        ->where('course_id', $courseId)
        ->get();

        // We make a HTML table
        $adminList = new AdminList($lessonsOut ?? []);

        $adminList->addColumn('check', 'course_id', '');

        $adminList->addColumn('thumbnail', 'image_src', trans('admin.thumbnail'));
        $adminList->addColumn('text', 'course_id', trans('admin.id'));
        $adminList->addColumn('bold-text', 'title', trans('admin.title'));
        $adminList->addColumn('status', 'status', trans('admin.status'));
        $adminList->addColumn('text', 'outstanding_order', trans('admin.outstanding_order'));
        $adminList->addColumn('text', 'created_at', trans('admin.published'));

        $adminList->addColumn('options', 'course_id', trans('admin.options'), [
            'elements' => [
                [
                    'title' => trans('admin.content_edit'),
                    'icon' => 'fa-solid fa-pen-to-square',
                    'onclick' => 'editCourseContent',
                    'onclick_params' => 'course_lesson_id'
                ],
                [
                    'title' => trans('admin.delete_content'),
                    'icon' => 'fa-solid fa-trash-can',
                    'onclick' => 'deleteCourseContent',
                    'onclick_params' => 'course_lesson_id'
                ]
            ]
        ]);

        $table = $adminList->getTable();

        // Edit modal
        $adminModal = new AdminModal("editCourseContentModal", 'Editar contenido curso', route('post.admin.course.content.by.id', ['courseId'=>$courseOut[0]->course_id]), 'modal-xl');

        // Start first column
        $adminModal->addInput('start_column', 'first_column', '');

        $adminModal->addInput('hidden', '_token', '', ['value'=>csrf_token()]);
        $adminModal->addInput('hidden', 'course_lesson_id', 'course_lesson_id');
        $adminModal->addInput('text', 'title', trans('admin.title'));
        $adminModal->addInput('text', 'description', trans('admin.description'));
        $adminModal->addInput('text', 'image_src', trans('admin.image_src'));
        $adminModal->addInput('text', 'outstanding_order', trans('admin.outstanding_order'), ['validation'=>'number']);

        // End first column & start second column
        $adminModal->addInput('end_column', 'first_column', '');
        $adminModal->addInput('start_column', 'second_column', '');

        $adminModal->addInput('select', 'is_active', trans('admin.is_active'), ['select'=>
            [0=>trans('admin.inactive'), 1=>trans('admin.active')]
        ]);

        $adminModal->addInput('select', 'lesson_type', trans('admin.lesson_type'), ['select'=>
            [1=>trans('admin.video'), 2=>trans('admin.text')]
        ]);

        $adminModal->addInput('text', 'lesson_data', trans('admin.video'));
        $adminModal->addInput('textarea', 'lesson_text', trans('admin.text'));
        $adminModal->addInput('submit', 'submit', trans('admin.update'));
 
        // End second column
        $adminModal->addInput('end_column', 'second_column', '');

        $modal = $adminModal->getModal();

        return view('admin.courses-contents-list', [
            'table'=>$table,
            'modal'=>$modal,
            'courseOut'=>$courseOut[0]
        ]);
        
    }

    public function getCourseById($courseId){

        $course = Course::where('course_id', $courseId)
        ->take(1)
        ->get();

        return response()->json(['success'=>isset($course[0]), 'result'=>$course[0] ?? null]);

    }

    public function getCourseContentById($courseId, $contentId){

        $lesson = CourseLesson::where('course_id', $courseId)
        ->where('course_lesson_id', $contentId)
        ->take(1)
        ->get();

        return response()->json(['success'=>isset($lesson[0]), 'result'=>$lesson[0] ?? null]);

    }

    public function postUpdateCourseById(Request $request){

        Course::where('course_id', $request->input('course_id'))
        ->update([
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'slug'=>$request->input('slug'),
            'is_active'=>$request->input('is_active'),
            'image_src'=>$request->input('image_src'),
            'outstanding_order'=>$request->input('outstanding_order')
        ]);

        return response()->json(['success'=>true]);
        
    }

    public function deleteCourseById(Request $request){

        CourseLesson::where('course_id', $request->input('course_id'))
        ->delete();

        Course::where('course_id', $request->input('course_id'))
        ->delete();

        return response()->json(['success'=>true]);

    }

    public function putCreateCourseContent($courseId){
       
        CourseLesson::create([
            'slug'=>'change-me-'.Str::random(6),
            'title'=>'',
            'description'=>'',
            'is_active'=>false,
            'course_id'=>$courseId,
            'lesson_type'=>1
        ]);

        return redirect()->route('get.admin.course.content.by.id', ['courseId'=>$courseId]);

    }

    public function postUpdateCourseContentById(Request $request, $courseId){

        if($request->input('lesson_data')!=null){
            $lessonData = json_encode($request->input('lesson_data'));
        }

        CourseLesson::where('course_id', $courseId)
        ->where('course_lesson_id', $request->input('course_lesson_id'))
        ->update([
            'title'=>$request->input('title') ?? '',
            'description'=>$request->input('description'),
            'is_active'=>$request->input('is_active'),
            'image_src'=>$request->input('image_src'),
            'outstanding_order'=>$request->input('outstanding_order'),
            'lesson_type'=>$request->input('lesson_type'),
            'lesson_text'=>$request->input('lesson_text'),
            'lesson_data'=>$lessonData ?? null,
        ]);

        return response()->json(['success'=>true]);
        
    }

    public function deleteCourseContentById(Request $request){

        CourseLesson::where('course_lesson_id', $request->input('course_lesson_id'))
        ->delete();

        return response()->json(['success'=>true, 'course_lesson_id'=>$request->input('course_lesson_id')]);
        
    }
    
}
