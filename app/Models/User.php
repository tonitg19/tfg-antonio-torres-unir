<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    // Table
    protected $table = 'users';

    // PrimaryKey
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'wallet',
        'level',
        'xp_points'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [

    ];

    // Return the user level by xp points
    public static function calculateUserLevelByXpPoints($xpPoints){

        $normalization = $xpPoints*0.000006989;
        return floor((99*$normalization) / sqrt(pow($normalization, 2) + 1) + 1);

    }

    public function getUserLevel(){
        return Self::calculateUserLevelByXpPoints($this->xp_points);
    }

}
