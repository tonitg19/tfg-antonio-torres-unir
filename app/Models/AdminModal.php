<?php

namespace App\Models;
use Illuminate\Support\Str;

class AdminModal
{

    protected $inputs = [];
    protected $title;
    protected $submitUrl;
    protected $id;
    protected $size;

    // Construct
    public function __construct($id, $title, $submitUrl, $size){

        $this->title = $title;
        $this->submitUrl = $submitUrl;
        $this->id = $id;
        $this->size = $size;

    }

    // Modal
    public function getModal(){


        // Start capturing the output
        ob_start();

        echo '<div class="modal fade" id="'.$this->id.'">
        <div class="modal-dialog '.$this->size.'" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">'.$this->title.'</h5>
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                </div>
                <div class="modal-body">
                    <form method="POST" id="'.$this->id.'-form" action="'.$this->submitUrl.'" enctype="multipart/form-data" class="form-validate is-alter">
                        <div class="row align-items-start">
        ';

        foreach($this->inputs as $input){

            $functionName = Str::camel('get '.$input['type'].' Input');
            echo call_user_func(['self', $functionName], $input);

        }

        echo '
            </div>
            </form>
                    </div>
                </div>
            </div>
        </div>';

        $out = ob_get_clean();

        return $out;

    }

    // Add input
    public function addInput($type, $key, $title, $options=null){
        $this->inputs[] = [
            'type'=>$type,
            'key'=>$key,
            'title'=>$title,
            'options'=>$options
        ];
    }

    private function getTextInput($input){

        $validation = $input['options']['validation'] ?? 'text';

        return '
        <div class="form-group" id="form-group-'.$input['key'].'">
            <label class="form-label" for="input-'.$input['key'].'">'.$input['title'].'</label>
            <div class="form-control-wrap">
                <input autocomplete="off" id="input-'.$input['key'].'" name="'.$input['key'].'" type="'.$validation.'" class="form-control" required>
            </div>
        </div>';

    }

    private function getTextareaInput($input){

        $validation = $input['options']['validation'] ?? 'text';

        return '
        <div class="form-group" id="form-group-'.$input['key'].'">
            <label class="form-label" for="input-'.$input['key'].'">'.$input['title'].'</label>
            <div class="form-control-wrap">
                <textarea autocomplete="off" id="input-'.$input['key'].'" name="'.$input['key'].'" type="'.$validation.'" class="form-control" required></textarea>
            </div>
        </div>';

    }

    private function getHiddenInput($input){

        $value = ($input['options']['value'] ?? '');

        return '<input type="hidden" id="input-'.$input['key'].'" name="'.$input['key'].'" value="'.$value.'">';

    }

    private function getSelectInput($input){

        $html = '
        <div class="form-group">
            <label class="form-label" for="input-'.$input['key'].'">'.$input['title'].'</label>
            <div class="form-control-wrap">
                <select id="input-'.$input['key'].'" name="'.$input['key'].'" class="form-control" required>
        ';

        foreach($input['options']['select'] as $i => $option){
            $html .= '<option value="'.$i.'">'.$option.'</option>';
        }

        $html .= '</select>
            </div>
        </div>';

        return $html;

    }

    private function getFileInput($input){

        $validation = $options['validation'] ?? 'text';

        return '
        <div class="form-group">
            <label class="form-label" for="input-'.$input['key'].'">'.$input['title'].'</label>
            <div class="form-control-wrap">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" autocomplete="off" id="input-'.$input['key'].'" name="'.$input['key'].'">
                    <label class="custom-file-label" for="input-'.$input['key'].'">Choose file</label>
                </div>
            </div>
        </div>';

    }

    private function getSubmitInput($input){

        return '
        <div class="form-group" style="margin-top:32px">
            <button disabled type="button" id="input-'.$input['key'].'" name="'.$input['key'].'" class="btn btn-xs btn-primary">'.$input['title'].'</button>
        </div>
        ';

    }

    private function getStartColumnInput($input){

        return '
            <div class="col">
        ';

    }

    private function getEndColumnInput($input){

        return '
            </div>
        ';

    }


}
