<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProgress extends Model
{
    use HasFactory;

    // Table
    protected $table = 'users_progress';

    // PrimaryKey
    protected $primaryKey = 'user_progress_id';

    // 
    protected $fillable = ['xp_points', 'course_lesson_id', 'user_id', 'course_id'];
}
