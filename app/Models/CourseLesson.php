<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ixudra\Curl\Facades\Curl;

use function PHPSTORM_META\map;

class CourseLesson extends Model
{
    use HasFactory;

     // Table
     protected $table = 'courses_lessons';

     // PrimaryKey
     protected $primaryKey = 'course_lesson_id';

    protected $fillable = [
        'course_id',
        'is_active',
        'description',
        'title',
        'lesson_type'
    ];

        
    public static function getVideoFromCloudflareStream($videoId){

        // Request to cloudflare to get the temporal token
        $response = Curl::to('https://api.cloudflare.com/client/v4/accounts/'
        .config('app.cloudflare_stream_account').'/stream/'.$videoId.'/token')
        ->withBearer(config('app.cloudflare_stream_bearer'))
        ->post();

        $token = json_decode($response, true)['result']['token'] ?? null;

        return '<iframe
        src="https://iframe.videodelivery.net/'.$token.'"
        style="border: none"
        height="711"
        width="1264"
        allow="accelerometer; gyroscope; autoplay; encrypted-media; picture-in-picture;"
        allowfullscreen="true">
        </iframe>';

    }

}
