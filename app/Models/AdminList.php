<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AdminList
{

    protected $columns;
    protected $data;

    // Construct
    public function __construct($data){

        $this->data = $data;

    }

    // Table
    public function getTable(){

        // Start capturing the output
        ob_start();

        // Table openings
        echo $this->getTableOpening();

        // Table columns
        foreach($this->columns as $column){
            print '<th scope="col">'.$this->getColumnTitle($column['title'], $column['type']).'</th>';
        }
        echo '
        </tr>
        </thead>
        <tbody>';

        // Content
        foreach($this->data as $row){

            echo '<tr>';

            foreach($this->columns as $column){

                $functionName = Str::camel('get '.$column['type'].' Column');

                echo '<td>';
                echo call_user_func(['self', $functionName], $row, $column);
                echo '</td>';

            }

            echo '</tr>';

        }

        // Clossing
        echo $this->getTableClosing();

        $out = ob_get_clean();

        return $out;

    }

    // Add column
    public function addColumn($type, $key, $title, $options=null){
        $this->columns[] = [
            'type'=>$type,
            'key'=>$key,
            'title'=>$title,
            'options'=>$options
        ];
    }

    // Get table opening
    private function getTableOpening(){

        $html = '
            <table class="table">
            <thead>
            <tr>
        ';

        return $html;

    }

    // Get column title
    private function getColumnTitle($title, $type=""){

        // If null, is the checkbox
        if($type=='check'){

            $html = '
            <div class="nk-tb-col nk-tb-col-check">
                <div class="custom-control custom-control-sm custom-checkbox notext">
                    <input type="checkbox" class="custom-control-input" id="uid">
                    <label class="custom-control-label" for="uid"></label>
                </div>
            </div>
            ';

        }elseif($type=='options'){

            if($title==null){
                $title = trans('admin.blog_entries_options_field');
            }

            $html = '
            <div class="text-right nk-tb-col nk-tb-col-tools">
                <span class="sub-text">'.$title.'</span>
            </div>
            ';

        }else{

            $html = '
                <div class="nk-tb-col tb-col-sm"><span class="sub-text">'.$title.'</span></div>
            ';

        }

        return $html ?? '';

    }

    // Get column closing
    private function getTableClosing(){

        $html = '
            </tbody>
            </table>
        ';

        return $html;

    }

    // COLUMNS TYPES

    // Get getCheckColumn
    private function getCheckColumn($row, $column){

        return '
        <div class="nk-tb-col nk-tb-col-check">
            <div class="custom-control custom-control-sm custom-checkbox notext">
            <input type="checkbox" class="custom-control-input" id="'.htmlspecialchars($row[$column['key']]).'">
                <label class="custom-control-label" for="'.htmlspecialchars($row[$column['key']]).'"></label>
            </div>
        </div>
        ';

    }

    // Get getTextColumn
    private function getTextColumn($row, $column){


        $html = '<div class="nk-tb-col tb-col-md">';
        $html .= htmlspecialchars($row[$column['key']]);

        if(isset($column['options']['filter'])){
            $html .= '<a class="brand-link" href="'.htmlspecialchars(route($column['options']['filter']['route'], [$column['options']['filter']['param']=>$row[$column['key']]])).'"> <em class="icon ni ni-filter-alt"></em></a>';
        }
    
        $html .= '</div>';

        return $html;

    }

    // Get getUrlColumn
    private function getUrlColumn($row, $column){
        return
        '<div class="nk-tb-col tb-col-md">
            <span>'.htmlspecialchars($row[$column['key']]).'</span>
            <a '.(isset($column['options']['external']) && $column['options']['external'] ? 'target="_blank"' : "").' class="brand-link" href="'.htmlspecialchars($row[$column['key']]).'">
                <em class="icon text-body fas fa-external-link-alt"></em>
            </a>
        </div>';
    }

    // Get getThumbnailColumn
    private function getThumbnailColumn($row, $column){

        $html = '
        <div class="nk-tb-col tb-col-sm">
            <div class="user-card">';

        if($row[$column['key']]!=null){
            $html.='<img height="64px" src="'.$row[$column['key']].'"/>';
        }else{
            $html.='<div style="height: 64px; width: 108.73px;" class="mr-3 user-avatar sq bg-warning-dim">
                <span>'.trans('admin.no_image').'</span>
            </div>';
        }

        $html .= '</div>
        </div>';

        return $html;

    }

    // Get getBoldTextColumn
    private function getBoldTextColumn($row, $column){

        if($row[$column['key']]==null){
            $text = '--';
        }else{
            $text = $row[$column['key']];
        }

        return '
        <div class="nk-tb-col tb-col-md">
            <div class="user-name">
                <span>'. htmlspecialchars($text) .'</span>
            </div>
        </div>
        ';

    }

    // Get getStatusColumn
    private function getStatusColumn($row, $column){

        $html = '
        <div class="nk-tb-col tb-col-lg">
            <ul class="list-status">';

            if($row['is_active']){
                $html .= '<span class="badge badge-dim badge-success">'.htmlspecialchars(trans('admin.active')).'</span>';
            }else{
                $html .= '<span class="badge badge-dim badge-danger">'.htmlspecialchars(trans('admin.inactive')).'</span>';
            }

        $html .= '
            </ul>
        </div>';

        return $html;

    }

    // Get getOptionsColumn
    private function getOptionsColumn($row, $column){

        $html = '<div class="nk-tb-col">';

        foreach($column['options']['elements'] as $element){

            // Has the user enought permission to see the button?
            if(isset($element['permission']) && !Auth::user()->can($element['permission'])){
                continue;
            }

            // Has the button a route?
            if(isset($element['route'])){
                $url = route($element['route'], $row[$column['key']]);
            }else{
                $url = '';
            }

            // Exists passed_url?
            if(isset($element['passed_url'])){
                $url = $row[$element['passed_url']];
            }

            // Has the button a onclick attr?
            if(isset($element['onclick'])){

                $onclick = $element['onclick'] . '(';

                $params = explode(',', $element['onclick_params']);

                foreach($params as $i => $param){
                    $onclick.= "'". $row[$param] . "'";

                    if($i+1!=count($params)){
                        $onclick.= ',';
                    }

                }

                $onclick.= ');';

            }else{
                $onclick = null;
            }

            $html .= '

                <a '.($onclick!=null ? 'onclick="'.$onclick.'"' : '').' '.($url!=null ? 'href="'.$url.'"' : '').' '.(isset($element['external']) && $element['external'] ? 'target="_blank"' : "").' class="btn btn-sm btn-icon btn-trigger" data-toggle="tooltip" data-placement="top" title="'.htmlspecialchars($element['title']).'">
                    <em class="icon ni '.$element['icon'].'"></em>
                </a>

            ';

        }

        $html .= '
        </div>';

        return $html;

    }


}
