<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CourseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', [CourseController::class, 'getHome'])->name('get.home');

// Course
Route::get('/course/{slug}/{lessonId?}', [CourseController::class, 'getCourseBySlug'])->name('get.course.by.slug');

// Login - Generate message
Route::get('/login/generate-message', [AuthController::class, 'getGenerateMesage'])->name('get.generate.message');

// Login - Check signature
Route::post('/login/check-signature', [AuthController::class, 'postCheckSignature'])->name('post.check.signature');

// Logout
Route::get('/logout', [AuthController::class, 'getLogout'])->name('get.logout');


// *** Admin Routes ***
Route::group(['middleware' => ['role:admin']], function (){

    // Admin Dashboard - Course list
    Route::get('/admin/courses', [AdminController::class, 'getAllCourses'])->name('get.admin.courses');

    // Admin Dashboard - Create course
    Route::put('/admin/courses/create', [AdminController::class, 'putCreateNewCourse'])->name('put.admin.courses');

    // Admin Dashboard - Get course by id
    Route::get('/admin/courses/id/{courseId}', [AdminController::class, 'getCourseById'])->name('get.admin.course.by.id');

    // Admin Dashboard - Content list
    Route::get('/admin/courses/id/{courseId}/content', [AdminController::class, 'getContentFromCourse'])->name('get.admin.course.content.by.id');

    // Admin Dashboard - Update a course
    Route::post('/admin/courses/update', [AdminController::class, 'postUpdateCourseById'])->name('post.admin.course.by.id');

    // Admin Dashboard - Delete a course
    Route::post('/admin/courses/delete', [AdminController::class, 'deleteCourseById'])->name('delete.admin.course');

    // Admin Dashboard - Create a new content
    Route::put('/admin/courses/id/{courseId}/content/create', [AdminController::class, 'putCreateCourseContent'])->name('put.admin.course.content.by.id');

    // Admin Dashboard - Update a content
    Route::post('/admin/courses/id/{courseId}/content/update', [AdminController::class, 'postUpdateCourseContentById'])->name('post.admin.course.content.by.id');

    // Admin Dashboard - Delete a content
    Route::delete('/admin/courses/id/{courseId}/content/delete', [AdminController::class, 'deleteCourseContentById'])->name('delete.admin.course.content');

    // Admin Dashboard - Get course content by id
    Route::get('/admin/courses/id/{courseId}/content/{contentId}', [AdminController::class, 'getCourseContentById'])->name('get.admin.course.content.to.edit.by.id');

});