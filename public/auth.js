document.addEventListener('DOMContentLoaded' , function() {
    const loginInWithEthereum = document.createElement( 'BUTTON' );
    //loginInWithEthereum.innerHTML = ethsvg + "<div style='line-height:30px; text-transform:uppercase; float:right; font-weight:bold'>Sign In with Ethereum</div>";
    loginInWithEthereum.innerText = "Web3 Login";
    loginInWithEthereum.className = 'btn btn-outline-blue me-2'; // First 2 classes are defined in WP
    loginInWithEthereum.addEventListener( 'click', triggerEthereumLogin );
    loginInWithEthereum.style.display = 'block';
    const currentLoginForm = document.getElementById( 'loginform' );
    currentLoginForm.insertAdjacentElement( 'afterend', loginInWithEthereum );

} );

function errorMessage(text) {
    const divAlertContainer = document.getElementById('alert-container');
    divAlertContainer.classList.add('alert');
    divAlertContainer.classList.add('alert-danger');
    divAlertContainer.innerText = text;
}

/**
* User clicked "Log In with Ethereum";
*/
function triggerEthereumLogin() {
    if ( ! window.ethereum ) {
        //TODO proper error message
        errorMessage( 'You need an Ethereum wallet installed as a browser extension. You can try Metamask or Coinbase Wallet extensions.' );
        return;
    }
    let nonce = '';
    let address = '';

    window.ethereum.request( { method: 'eth_requestAccounts' } )
    .then( ret => fetch(
    "{{ route('get.generate.message') }}?wallet=" + ret[0], {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    }
    ))
    .then( messageToSign => messageToSign.json())
    .then( messageToSign => {

        console.log(messageToSign.address)

        address = messageToSign.address;
        nonce = messageToSign.nonce;
        encrypted_message = messageToSign.encrypted_message;

        return window.ethereum.request( {
            method: 'personal_sign',
            params: [
            `0x${toHex( messageToSign.message )}`,
            messageToSign.address,
            ],
        } )
    } )
    .then( signature => {

        fetch(
        "{{ route('post.check.signature') }}", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                wallet: address,
                nonce: nonce,
                signature: signature,
                encrypted_message : encrypted_message,
                _token: "{{ csrf_token() }}"
            }),
            credentials: 'include'
        }
        )
        .then( signature => signature.json())
        .then( signature => {
            if(signature.result=='ok'){
                location.reload();
            }
        });


    } )
    .catch( err => {
        if ( err.code && err.code === 4001 ) {
            errorMessage( 'You have to confirm the signature. Please try again and confirm the sign request this time.' );
        } else if ( err.message ) {
            errorMessage( err.message );
        }
    } );
}

function toHex( stringToConvert ) {
    return stringToConvert
    .split('')
    .map((c) => c.charCodeAt(0).toString(16).padStart(2, '0'))
    .join('');
}

function addLoginData( name, value ) {
    const element = document.createElement( 'INPUT' );
    element.setAttribute( 'type', 'hiddden' );
    element.setAttribute( 'name', name );
    element.value = value;
    return element;
}