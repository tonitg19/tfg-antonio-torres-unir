<?php return [


    'home_button' => 'Inicio',
    'admin_button' => 'Administración',
    'logout_button' => 'Cerrar sesión',

    'login_description' => 'Vas a utilizar tu billetera para verificar tu identidad.',


    'footer_title' => 'Plataforma de formación',
    'footer_description' => 'Trabajo final de grado',
    'footer_text' => 'Web realizada por Antonio Torres García como trabajo final de grado en Ingeniería Informática por la Universidad Internacional de la Rioja',


    'banner_title' => 'Aprende de manera fácil',
    'banner_description' => 'Tienes disponible diferentes cursos con los que podrás aprender tecnologías revolucionarias.',

    'level' => 'Nivel: :level',
    'points' => 'Puntos: :points',

    'avaiable_courses' => 'Cursos disponibles',

    'completed' => 'Completado',
    'not_completed' => 'Pendiente',

    'signout' => 'Cerrar sesión',

    'not_login_error' => 'Es necesario haber iniciado sesión para poder ver esta sección.',
    'course_not_exist_error' => 'El contenido que estás buscando no existe',

];
