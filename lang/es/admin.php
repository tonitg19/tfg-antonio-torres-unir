<?php return [

    'published'=>'Creación',
    'status'=>'Estado',
    'title'=>'Título',
    'id'=>'Id',
    'thumbnail'=>'Miniatura',
    'options'=>'Opciones',

    'is_active' => 'Estado',
    'active'=>'Activo',
    'inactive'=>'Inactivo',

    'new_course' => 'Nuevo curso',

    'no_image' => 'Sin miniatura',

    'update' => 'Actualizar',


    'image_src' => 'Url miniatura',

    'description' => 'Descripción',


    'outstanding_order' => 'Orden',


    'slug' => 'Slug',



    'new_content' => 'Añadir contenido',
    'text' => 'Texto',

    'video' => 'Video',


    'lesson_type' => 'Tipo de lección',


];