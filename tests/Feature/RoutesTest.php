<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;

class RoutesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_check_application_routes()
    {

        // Check homepage
        $response = $this->get('/');
        $response->assertStatus(200);

        // Check one published course, 302 expected WITHOUT authentication
        $course = Course::select('slug')
        ->where('is_active', true)
        ->take(1)
        ->get();
        
        $response = $this->get('/course/'.$course[0]->slug);
        $response->assertStatus(302);

        // Check one published course, 200 expected WITH authentication
        Auth::loginUsingId(1);
        $response = $this->get('/course/'.$course[0]->slug);
        $response->assertStatus(200);

        // Check generate message with a valid wallet
        $response = $this->get('/login/generate-message?wallet=0x94beba004c234cc0e514224b55e761111786c94e');
        $response->assertStatus(200);

    }
}