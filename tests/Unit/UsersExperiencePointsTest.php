<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;

class UsersExperiencePointsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_levels_by_xp_points()
    {

        $level = User::calculateUserLevelByXpPoints(100000);

        // For 100000 points, the expected user level is 57.
        $this->assertEquals(57, $level);

        $level = User::calculateUserLevelByXpPoints(76000);

        // For 76000 points, the expected user level is 47.
        $this->assertEquals(47, $level);

        $level = User::calculateUserLevelByXpPoints(115000);

        // For 115000 points, the expected user level is 63.
        $this->assertEquals(63, $level);

        $level = User::calculateUserLevelByXpPoints(1000);

        // For 1000 points, the expected user level is 1.
        $this->assertEquals(1, $level);

        $level = User::calculateUserLevelByXpPoints(5000);

        // For 5000 points, the expected user level is 4.
        $this->assertEquals(4, $level);

    }
}
