<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Helpers\Web3;
use Illuminate\Support\Facades\Crypt;

class SignVerificationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_checkValidSignature()
    {

        $wallet = '0x94beba004c234cc0e514224b55e761111786c94e';
        $signature = '0x2f7ea3cf9f0018898580b14fd01ea71a838c1ef359e7d993f072333c30415ab50835ec4a2b8cf3ed1b97f2ae747a3cf2288f2601559afb8bcb4fb55164af9a2b1b';
        $message = decrypt(
            'eyJpdiI6IjdRWERybzJBVzU0eFREWk9rVE5saHc9PSIsInZhbHVlIjoiNVpUTS8xdG1GM1h4SkMwbFczdG8yTURoU05jM2N3ciswOFh1QkpkTTdJaUtoZFFPVk1nZ0pFTGtRQkVBNEZJaGhMNWpOdzlOMUdpSUVNMGVKTEQ0U0NQL1JsSVdPbUM5by9pZnFMU1hEenVNeENxOTZnT3poWmRod1JuQkVSVDdwSmpTeXA4VEFWYktRdW8yVWsxVUtGRmY2NWlJRkdvckJubTVSQ1p6R3NPa3dJWEFtejRzamswYTdZdC9Mb0l2WTg3ME9KYXk1SFQ3bVVxYkxWVDZjZXczUTg1N0lWOFE3bGJ4Si9EQnFvUC9pcXkwVXBLeXRXNGVYQkE0aElHNkV6RkNHWjJWVzFrWVdCbTRvckJMZi8rZjI0eHdIWm1Mc0l2YkhYWXdEWnNHRlhRUXVZdjV3UW05RWRzMEVBMVhiclQyUzZseWI2MVlkNWlxUTJUTVRhRlFBQkRQWjZVY0NvV3Y5Mk1VTks1OGFWbUFUK0dhNVlWbmVDb0ovZWdBU09DbmdSYkF3Yk5NclVLcXlpa0pmL1JEQkp2ZVcvRFRKRll2ZlpBR2lvREFCUkhXaFpuei9tZjJoKzZYUG92UnVBL2UvNEhmSWdnV24zYlRXVElRQVE9PSIsIm1hYyI6IjE1ZTI4NWJmOGE1Njc2NDQyZGZmYWJiN2QzNDliMjYyMjRkZDEzMDdlZDNhODBkM2U1NjEwY2Q5ZjFjZmUxZWMiLCJ0YWciOiIifQ=='
        );

        $verify = Web3::verify_signature($message, $signature, $wallet);

        $this->assertTrue($verify);

    }

    public function test_checkNotValidSignature()
    {
        $wallet = '0x94beba004c234cc0e514224b55e761111786c94e';
        $signature = '0x1f7ea3cf9f0018898580b14fd01ea71a838c1ef359e7d993f072333c30415ab50835ec4a2b8cf3ed1b97f2ae747a3cf2288f2601559afb8bcb4fb55164af9a2b1b';
        $message = decrypt(
            'eyJpdiI6IjdRWERybzJBVzU0eFREWk9rVE5saHc9PSIsInZhbHVlIjoiNVpUTS8xdG1GM1h4SkMwbFczdG8yTURoU05jM2N3ciswOFh1QkpkTTdJaUtoZFFPVk1nZ0pFTGtRQkVBNEZJaGhMNWpOdzlOMUdpSUVNMGVKTEQ0U0NQL1JsSVdPbUM5by9pZnFMU1hEenVNeENxOTZnT3poWmRod1JuQkVSVDdwSmpTeXA4VEFWYktRdW8yVWsxVUtGRmY2NWlJRkdvckJubTVSQ1p6R3NPa3dJWEFtejRzamswYTdZdC9Mb0l2WTg3ME9KYXk1SFQ3bVVxYkxWVDZjZXczUTg1N0lWOFE3bGJ4Si9EQnFvUC9pcXkwVXBLeXRXNGVYQkE0aElHNkV6RkNHWjJWVzFrWVdCbTRvckJMZi8rZjI0eHdIWm1Mc0l2YkhYWXdEWnNHRlhRUXVZdjV3UW05RWRzMEVBMVhiclQyUzZseWI2MVlkNWlxUTJUTVRhRlFBQkRQWjZVY0NvV3Y5Mk1VTks1OGFWbUFUK0dhNVlWbmVDb0ovZWdBU09DbmdSYkF3Yk5NclVLcXlpa0pmL1JEQkp2ZVcvRFRKRll2ZlpBR2lvREFCUkhXaFpuei9tZjJoKzZYUG92UnVBL2UvNEhmSWdnV24zYlRXVElRQVE9PSIsIm1hYyI6IjE1ZTI4NWJmOGE1Njc2NDQyZGZmYWJiN2QzNDliMjYyMjRkZDEzMDdlZDNhODBkM2U1NjEwY2Q5ZjFjZmUxZWMiLCJ0YWciOiIifQ=='
        );

        $verify = Web3::verify_signature($message, $signature, $wallet);

        $this->assertFalse($verify);
    }
}
