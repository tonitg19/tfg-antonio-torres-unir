<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('theme.css') }}">
    <title>{{ 'Plataforma de formación' }}</title>

</head>
<body class="d-flex flex-column h-100">

    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ route('get.home') }}">
                    <img src="{{ asset('tfg-logo.png') }}">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">

                    <ul class="navbar-nav me-auto mb-2 mb-md-0">
                        <li class="nav-item">
                            <a class="nav-link active text-black" aria-current="page" href="{{ route('get.home') }}">{{ trans('web.home_button') }}</a>
                        </li>
                        @role('admin')
                        <li class="nav-item">
                            <a class="nav-link active text-black" aria-current="page" href="{{ route('get.admin.courses') }}">{{ trans('web.admin_button') }}</a>
                        </li>
                        @endrole
                    </ul>

                    @if (Auth::user())

                    <div class="dropdown text-end">
                        <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="https://avatar.tobi.sh/{{ Auth::user()->wallet }}.svg?text=%23{{ Auth::user()->getUserLevel() }}" alt="mdo" width="32" height="32" class="rounded-circle">
                        </a>
                        <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1" style="">
                            <li><a class="dropdown-item">{{ Auth::user()->wallet }}</a></li>
                            <li><a class="dropdown-item">{{ trans('web.level', ['level'=>Auth::user()->getUserLevel()]) }}</a></li>
                            <li><a class="dropdown-item">{{ trans('web.points', ['points'=>Auth::user()['xp_points']]) }}</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ route('get.logout') }}">{{ trans('web.signout') }}</a></li>
                        </ul>
                    </div>

                    @else
                    <form id="loginform" class="d-flex">

                    </form>
                    @endif

                </div>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer class="bg-blue text-white text-center text-lg-start">
        <!-- Grid container -->
        <div class="container p-4">
          <!--Grid row-->
          <div class="row">
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
              <h5>{{ trans('web.footer_title') }}</h5>
              <p>
                {{ trans('web.footer_description') }}
              </p>
            </div>

          </div>
          <!--Grid row-->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            {{ trans('web.footer_text') }}
        </div>
        <!-- Copyright -->
      </footer>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script>

        document.addEventListener('DOMContentLoaded' , function() {
            const loginInWithEthereum = document.createElement( 'BUTTON' );
            //loginInWithEthereum.innerHTML = ethsvg + "<div style='line-height:30px; text-transform:uppercase; float:right; font-weight:bold'>Sign In with Ethereum</div>";
            loginInWithEthereum.innerText = "Web3 Login";
            loginInWithEthereum.className = 'btn btn-outline-blue me-2'; // First 2 classes are defined in WP
            loginInWithEthereum.addEventListener( 'click', triggerEthereumLogin );
            loginInWithEthereum.style.display = 'block';
            const currentLoginForm = document.getElementById( 'loginform' );
            currentLoginForm.insertAdjacentElement( 'afterend', loginInWithEthereum );

        } );

        function errorMessage(text) {
            const divAlertContainer = document.getElementById('alert-container');
            divAlertContainer.classList.add('alert');
            divAlertContainer.classList.add('alert-danger');
            divAlertContainer.innerText = text;
        }

        /**
        * User clicked "Log In with Ethereum";
        */
        function triggerEthereumLogin() {
            if ( ! window.ethereum ) {
                //TODO proper error message
                errorMessage( 'You need an Ethereum wallet installed as a browser extension. You can try Metamask or Coinbase Wallet extensions.' );
                return;
            }
            let nonce = '';
            let address = '';

            window.ethereum.request( { method: 'eth_requestAccounts' } )
            .then( ret => fetch(
            "{{ route('get.generate.message') }}?wallet=" + ret[0], {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            }
            ))
            .then( messageToSign => messageToSign.json())
            .then( messageToSign => {

                console.log(messageToSign.address)

                address = messageToSign.address;
                nonce = messageToSign.nonce;
                encrypted_message = messageToSign.encrypted_message;

                return window.ethereum.request( {
                    method: 'personal_sign',
                    params: [
                    `0x${toHex( messageToSign.message )}`,
                    messageToSign.address,
                    ],
                } )
            } )
            .then( signature => {

                fetch(
                "{{ route('post.check.signature') }}", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        wallet: address,
                        nonce: nonce,
                        signature: signature,
                        encrypted_message : encrypted_message,
                        _token: "{{ csrf_token() }}"
                    }),
                    credentials: 'include'
                }
                )
                .then( signature => signature.json())
                .then( signature => {
                    if(signature.result=='ok'){
                        location.reload();
                    }
                });


            } )
            .catch( err => {
                if ( err.code && err.code === 4001 ) {
                    errorMessage( 'You have to confirm the signature. Please try again and confirm the sign request this time.' );
                } else if ( err.message ) {
                    errorMessage( err.message );
                }
            } );
        }

        function toHex( stringToConvert ) {
            return stringToConvert
            .split('')
            .map((c) => c.charCodeAt(0).toString(16).padStart(2, '0'))
            .join('');
        }

        function addLoginData( name, value ) {
            const element = document.createElement( 'INPUT' );
            element.setAttribute( 'type', 'hiddden' );
            element.setAttribute( 'name', name );
            element.value = value;
            return element;
        }

    </script>

    @yield('javascript')

</body>
</html>
