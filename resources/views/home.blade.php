@extends('layout')
@section('content')

    <main class="flex-shrink-0 container">

        <div id="alert-container" role="alert">

        </div>

        <div class="d-flex align-items-center p-3 my-3 text-white bg-blue rounded shadow-sm">
            <i class="fas fa-graduation-cap me-3"></i>
            <div class="lh-1">
                <h1 class="h6 mb-0 text-white lh-1">{{ trans('web.banner_title') }}</h1>
                <small>{{ trans('web.banner_description') }}</small>
            </div>
        </div>

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <h6 class="border-bottom pb-2 mb-0">{{ trans('web.avaiable_courses') }}</h6>


            @foreach ($courses as $course)
            <div class="d-flex text-muted pt-3">
                <a href="{{ route('get.course.by.slug', ['slug'=>$course->slug]) }}">
                    <img class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="300" height="150" src="{{ $course->image_src }}">
                </a>

                <p class="pb-3 mb-0 small lh-sm border-bottom fs-5">
                    <a class="text-decoration-none" href="{{ route('get.course.by.slug', ['slug'=>$course->slug]) }}">
                        <strong class="d-block text-gray-dark fs-5 text-blue">{{ $course->title }}</strong>
                    </a>
                    {{ $course->description }}
                </p>

                @if(isset($course->status))

                    @if($course->status==1)
                        <div><span class="badge badge-success">{{ trans('web.completed') }}</span></div>
                    @else
                        <div><span class="badge badge-secondary">{{ trans('web.not_completed') }}</span></div>
                    @endif

                @endif

            </div>
            @endforeach
        </div>

    </main>
@endsection
