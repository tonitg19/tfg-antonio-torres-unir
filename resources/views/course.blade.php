@extends('layout')
@section('content')

    <main class="flex-shrink-0 container">

        <div class="d-flex align-items-center p-3 my-3 text-white bg-blue rounded shadow-sm">
            <i class="fas fa-graduation-cap me-3"></i>
            <div class="lh-1">
                <h1 class="h6 mb-0 text-white lh-1">{{ trans('web.banner_title') }}</h1>
                <small>{{ trans('web.banner_description') }}</small>
            </div>
        </div>

        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <h5 class="border-bottom pb-2 mb-0">{{ $course->title }} > {{ $currentLesson->title }}</h5>

            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{ $percentage }}%;" aria-valuenow="{{ $percentage }}" aria-valuemin="0" aria-valuemax="100">{{ $percentage }}%</div>
            </div>

            <div class="content-container">
                {!! $htmlContent !!}
            </div>

            <div class="course-options row">
                <div class="col-md-6">
                    @if($previousLesson!=null)
                        <a class="btn btn-primary previous-button" href="{{ route('get.course.by.slug', ['slug'=>$course->slug, 'lessonId'=>$previousLesson->course_lesson_id]) }}" role="button">{{ trans('pagination.previous') }}: {{ $previousLesson->title }}</a>
                    @endif
                </div>
                <div class="col-md-6">
                    @if($nextLesson!=null)
                        <a class="btn btn-primary next-button" href="{{ route('get.course.by.slug', ['slug'=>$course->slug, 'lessonId'=>$nextLesson->course_lesson_id]) }}" role="button">{{ trans('pagination.next') }}: {{ $nextLesson->title }}</a>
                    @endif
                </div>
            </div>

            <div class="course-description row">

                <div class="col-md-12">

                    <h2>{{ $currentLesson->title }}</h2>

                    <p>{{ $currentLesson->description }}</p>

                </div>
                
            </div>

        </div>

    </main>
@endsection
