@extends('layout')
@section('content')

    <main class="flex-shrink-0 container">

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <h6 class="border-bottom pb-2 mb-0">Admin panel</h6>

            <div style="margin: 10px 0px 10px 0px">
                <form action="{{ route('put.admin.course.content.by.id', ['courseId'=>$courseOut->course_id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <button class="btn btn-outline-blue me-2" type="submit">{{ trans('admin.new_content') }}</button>
                </form>
            </div>

            <div class="card-inner p-0">
                {!! $table !!}
            </div>
            
        </div>

    </main>

    {!! $modal !!}

@endsection
@section('javascript')
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>



<script>

function deleteCourseContent(lessonId){

    fetch('{{ route('delete.admin.course.content', ['courseId'=>$courseOut->course_id]) }}', {
        method: 'DELETE',
        body: JSON.stringify({
            'course_lesson_id':lessonId
        }),
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}",
            "content-type": "application/json"
        }
    })
    .then(res => res.text()) // or res.json()
    .then(res => console.log(res))
    .then(function(){

       window.location.reload();
        
    })

}

function editCourseContent(lessonId){

$('#input-submit').prop('disabled', true);
$('#editCourseContentModal').modal('show');

var url = "{{ route('get.admin.course.content.to.edit.by.id', ['contentId'=>'_', 'courseId'=>$courseOut->course_id]) }}";
url = url.slice(0, -1) + lessonId;

$.ajax({
    url:url,
    type: 'GET',
    success: function( data, b){

        if(data.success){
            console.log(data);
            $('#input-course_lesson_id').val(data.result.course_lesson_id);
            $('#input-title').val(data.result.title);
            $('#input-description').val(data.result.description);
            $('#input-is_active').val(Number(data.result.is_active));
            $('#input-outstanding_order').val(data.result.outstanding_order);
            $('#input-image_src').val(data.result.image_src);
            $('#input-lesson_type').val(data.result.lesson_type);
            $('#input-lesson_data').val(data.result.lesson_data);
            $('#input-lesson_text').val(data.result.lesson_text);
            $('#input-submit').attr('onclick', 'editCourseContentPost()');
            $('#input-submit').prop('disabled', false);
        }

    }
});

}


function editCourseContentPost(){

$('#input-submit').prop('disabled', true);

data = new FormData($('form#editCourseContentModal-form')[0]);

$.ajax({
    url: $('form#editCourseContentModal-form').attr('action'),
    data: data,
    type: 'POST',
    cache:false,
    contentType: false,
    processData: false,
    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
    success: function( data, b){

        if(data.success){

            $('#editCourseContentModal').modal('hide');

            location.reload();

        }else{

            $('#input-submit').prop('disabled', false);

        }

    }

});

}

</script>

@endsection
