@extends('layout')
@section('content')

    <main class="flex-shrink-0 container">

        @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif

        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <h6 class="border-bottom pb-2 mb-0">Admin panel</h6>

            <div style="margin: 10px 0px 10px 0px">
                <form action="{{ route('put.admin.courses') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <button class="btn btn-outline-blue me-2" type="submit">{{ trans('admin.new_course') }}</button>
                </form>
            </div>

            <div class="card-inner p-0">
                {!! $table !!}
            </div>
            
        </div>

    </main>

    {!! $modal !!}

@endsection
@section('javascript')
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>



<script>

function deleteCourse(courseId){

    fetch('{{ route('delete.admin.course') }}', {
        method: 'POST',
        body: JSON.stringify({
            'course_id':courseId
        }),
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}",
            "content-type": "application/json"
        }
    })
    .then(res => res.text()) // or res.json()
    .then(res => console.log(res))
    .then(function(){

       window.location.reload();
        
    })

}

function editCourse(courseId){

$('#input-submit').prop('disabled', true);
$('#editCourseModal').modal('show');

var url = "{{ route('get.admin.course.by.id', ['courseId'=>'_']) }}";
url = url.slice(0, -1) + courseId;

$.ajax({
    url:url,
    type: 'GET',
    success: function( data, b){

        if(data.success){
            console.log(data);
            $('#input-course_id').val(data.result.course_id);
            $('#input-title').val(data.result.title);
            $('#input-slug').val(data.result.slug);
            $('#input-description').val(data.result.description);
            $('#input-is_active').val(Number(data.result.is_active));
            $('#input-outstanding_order').val(data.result.outstanding_order);
            $('#input-image_src').val(data.result.image_src);
            $('#input-submit').attr('onclick', 'editCoursePost()');
            $('#input-submit').prop('disabled', false);
        }

    }
});

}


function editCoursePost(){

$('#input-submit').prop('disabled', true);

data = new FormData($('form#editCourseModal-form')[0]);

$.ajax({
    url: $('form#editCourseModal-form').attr('action'),
    data: data,
    type: 'POST',
    cache:false,
    contentType: false,
    processData: false,
    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
    success: function( data, b){

        if(data.success){

            $('#editCourseModal').modal('hide');

            location.reload();

        }else{

            $('#input-submit').prop('disabled', false);

        }

    }

});

}

</script>

@endsection
