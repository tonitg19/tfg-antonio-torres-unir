<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\CourseLesson;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $courses[] = Course::insertGetId([
            'title'=>'Curso de introducción a blockchain',
            'description'=>'Proin in interdum dolor. Mauris vitae sapien sed tellus fermentum efficitur eget quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi fermentum mattis quam eget laoreet.',
            'slug'=>'curso-introduccion-blockchain',
            'is_active'=>1,
            'image_src'=>'https://i.picsum.photos/id/142/200/200.jpg?hmac=L8yY8tFPavTj32ZpuPiqsLsfWgDvW1jvoJ0ETDOUMGg',
            'outstanding_order'=>1
        ]);

        $courses[] = Course::insertGetId([
            'title'=>'Curso de creación de validador en Cosmos',
            'description'=>'Proin in interdum dolor. Mauris vitae sapien sed tellus fermentum efficitur eget quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi fermentum mattis quam eget laoreet.',
            'slug'=>'curso-creacion-validador-en-cosmos',
            'is_active'=>1,
            'image_src'=>'https://i.picsum.photos/id/651/200/200.jpg?hmac=p8_kpEZVVgCD0ruS4M5WHOZ2-VETfCi3aXmYAbav3NE',
            'outstanding_order'=>2
        ]);

        $courses[] = Course::insertGetId([
            'title'=>'Curso de principios de web3',
            'description'=>'Proin in interdum dolor. Mauris vitae sapien sed tellus fermentum efficitur eget quis mi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Morbi fermentum mattis quam eget laoreet.',
            'slug'=>'curso-principios-de-web3',
            'is_active'=>1,
            'image_src'=>'https://i.picsum.photos/id/373/200/200.jpg?hmac=WAwyn7yIFXuyUxxF4b3ijw7qJfIP7oBXicnozVoLj_o',
            'outstanding_order'=>3
        ]);

        foreach($courses as $course){

            CourseLesson::insert([
                'title'=>'Lección 1 - Lorem Ipsum',
                'description'=>'Nulla imperdiet dictum leo vitae feugiat. Quisque non lorem in elit aliquam iaculis. Sed sapien mi, pretium quis blandit non, sagittis quis ex. Sed sed arcu a nisl gravida volutpat eu id nulla.',
                'is_active'=>true,
                'course_id'=>$course,
                'image_src'=>'https://i.picsum.photos/id/1040/536/354.jpg?hmac=BqQeti4nsnWLbyiaS4HHRq3V-sdsNFsCzif-V7I2WP4',
                'outstanding_order'=>1,
                'lesson_type'=>1,
                'lesson_data'=>json_encode(['id'=>'d8cd2e7929cccd63d8809ed858abdf2d']),
                'lesson_text'=>null
            ]);

            CourseLesson::insert([
                'title'=>'Lección 2 - Lorem Ipsum',
                'description'=>'Nulla imperdiet dictum leo vitae feugiat. Quisque non lorem in elit aliquam iaculis. Sed sapien mi, pretium quis blandit non, sagittis quis ex. Sed sed arcu a nisl gravida volutpat eu id nulla.',
                'is_active'=>true,
                'course_id'=>$course,
                'image_src'=>'https://i.picsum.photos/id/273/536/354.jpg?hmac=XeIXBIGkzMHse-G_bjmKvNypzq7R4iFLS9eC_XhQu90',
                'outstanding_order'=>2,
                'lesson_type'=>1,
                'lesson_data'=>json_encode(['id'=>'d8cd2e7929cccd63d8809ed858abdf2d']),
                'lesson_text'=>null
            ]);

            CourseLesson::insert([
                'title'=>'Lección 3 - Lorem Ipsum',
                'description'=>'Nulla imperdiet dictum leo vitae feugiat. Quisque non lorem in elit aliquam iaculis. Sed sapien mi, pretium quis blandit non, sagittis quis ex. Sed sed arcu a nisl gravida volutpat eu id nulla.',
                'is_active'=>true,
                'course_id'=>$course,
                'image_src'=>'https://i.picsum.photos/id/419/536/354.jpg?hmac=Y6xemOYrr06vLtJ9r420XrZMuXG_nMj3nkwiKRgKiyU',
                'outstanding_order'=>3,
                'lesson_type'=>1,
                'lesson_data'=>json_encode(['id'=>'d8cd2e7929cccd63d8809ed858abdf2d']),
                'lesson_text'=>null
            ]);

            CourseLesson::insert([
                'title'=>'Lección 4 - Lorem Ipsum',
                'description'=>'Nulla imperdiet dictum leo vitae feugiat. Quisque non lorem in elit aliquam iaculis. Sed sapien mi, pretium quis blandit non, sagittis quis ex. Sed sed arcu a nisl gravida volutpat eu id nulla.',
                'is_active'=>true,
                'course_id'=>$course,
                'image_src'=>'https://i.picsum.photos/id/419/536/354.jpg?hmac=Y6xemOYrr06vLtJ9r420XrZMuXG_nMj3nkwiKRgKiyU',
                'outstanding_order'=>4,
                'lesson_type'=>2,
                'lesson_data'=>null,
                'lesson_text'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pretium convallis tellus sit amet convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla tellus ante, suscipit non luctus et, tincidunt quis arcu. Cras sollicitudin purus sit amet nulla condimentum tincidunt. Pellentesque mattis congue dapibus. Donec faucibus dignissim dapibus. Nam condimentum velit eget ipsum lobortis, ut pulvinar ante sagittis. Suspendisse faucibus elit in iaculis interdum. Proin et odio eget tortor dignissim placerat. Quisque eget semper erat, ac suscipit mauris. Maecenas sed justo rutrum, maximus eros ut, pharetra justo. Praesent magna eros, dignissim ac eros nec, interdum porta nulla. Mauris lacinia at elit sed blandit. Donec sit amet leo sed leo vestibulum ullamcorper et eu magna. Aliquam erat volutpat. Maecenas dapibus odio nec libero semper feugiat.</p>

                <p>Nunc vitae auctor tortor. Aliquam condimentum blandit lacus eget sollicitudin. Phasellus bibendum eget diam in viverra. Aenean et imperdiet lorem, sed ultricies odio. Praesent sit amet placerat nibh. Ut ultrices, nisi nec laoreet consequat, orci libero pretium tortor, quis elementum erat dolor et sem. Aliquam in elit sed ipsum aliquam mollis. Aliquam bibendum tempor lacus at faucibus. Etiam euismod vel dui sit amet scelerisque. Proin nec libero dolor.</p>
                
                <p>In aliquam pretium rhoncus. Aliquam euismod tincidunt orci, sed tempor libero tempus et. Nulla facilisi. Integer eget quam libero. Morbi in nunc sem. Integer sollicitudin urna tellus, in venenatis ipsum lacinia a. Etiam id tellus commodo justo convallis euismod vel eu arcu. Phasellus at posuere ante. Sed et enim ac sem ultricies feugiat. Sed vel scelerisque neque.</p>
                
                <p>Fusce ac ante vitae eros ullamcorper aliquam. Vivamus sollicitudin quam sit amet ante volutpat, eu ultricies eros elementum. Donec eros augue, sodales nec orci eget, pretium molestie augue. Aenean scelerisque ut est eu dignissim. Maecenas egestas maximus nibh, non condimentum tortor malesuada id. Duis consectetur semper neque. Curabitur efficitur, eros in pellentesque efficitur, velit nisl porta est, quis rutrum nibh massa id velit. Nam consequat arcu in libero bibendum vestibulum.</p>
                
                <p>Quisque varius ullamcorper ante, in venenatis turpis sodales a. Praesent placerat nulla eget ornare aliquet. Vivamus ultricies sed odio in fermentum. Curabitur eget est id metus dapibus vehicula eget at tortor. Aenean non convallis magna. Morbi congue enim in turpis sodales, ut iaculis justo elementum. Morbi vestibulum arcu id semper maximus. Nam id interdum sapien. Nullam aliquam sed justo vitae auctor. Proin convallis sem dui, at maximus ipsum lobortis eu. Nulla dapibus at est eu egestas. Etiam sit amet erat placerat, auctor libero a, dignissim sapien.</p>'
            ]);


        }

    }
}
