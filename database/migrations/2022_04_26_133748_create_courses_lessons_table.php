<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_lessons', function (Blueprint $table) {
            $table->integerIncrements("course_lesson_id")->unsigned();
            $table->string('title', 140);
            $table->string('description', 280);
            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('course_id')->on('courses')->onDelete('cascade')->onDelete('cascade');
            $table->boolean('is_active')->default(true);
            $table->string('image_src')->default(null)->nullable();
            $table->integer('outstanding_order')->default(0);
            $table->tinyInteger('lesson_type')->unsigned();
            $table->json('lesson_data')->nullable();
            $table->text('lesson_text')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
};
