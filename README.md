Es necesario PHP 8.1

#### Dependencies
```
composer install
```

#### Start server
```
php artisan serve --port 80
```

http://localhost:80